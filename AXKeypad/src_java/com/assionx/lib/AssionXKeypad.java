package com.assionx.lib;

public class AssionXKeypad 
{
	public static native void onKeyDown(int keyCode);
	
	public static native void onKeyUp(int keyCode);
	
	public static native void onKeyLongPressed(int keyCode);
}
