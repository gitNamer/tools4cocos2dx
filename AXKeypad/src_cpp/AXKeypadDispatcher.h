/*
 * AXKeypadDispatcher.h
 *
 *  Created on: 2014年5月17日
 *      Author: coldwinter
 */

#ifndef AXKEYPADDISPATCHER_H_
#define AXKEYPADDISPATCHER_H_

#include <cocos2d.h>
using namespace cocos2d;

#include "AXHandler.h"

class AXKeypadDelegate;
class AXKeypadDispatcher:public CCObject
{
public:
	typedef enum __keypad_action
	 {  
	 	kAXKeyDown,
	 	kAXKeyUp
	 }KeyAction;
	AXKeypadDispatcher();
	static AXKeypadDispatcher* sharedKeypadDispatcher();

	void registKeypadDispatcher(AXKeypadDelegate*);

	void unregistKeypadDispatcher(AXKeypadDelegate*);

	void handleKeypadMSG(int keycode,int action);
    
    void postKeyEvent(int keyCode,int action);
    
    void destroy();

protected:
    static AXKeypadDispatcher* g_instance;
	CCArray* m_key_handler;

	CCArray* m_added;
	CCArray* m_removed;

	bool m_block;
    
    AXHandler* m_handler;

	static char keymap[256];

	virtual ~AXKeypadDispatcher();

	int getIndexInArray(CCArray*, AXKeypadDelegate*);
    
    void handleMessage(Message);
};

#endif /* AXKEYPADDISPATCHER_H_ */
