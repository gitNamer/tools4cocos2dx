/*
 * AXKeypadDelegate.h
 *
 *  Created on: 2014年5月16日
 *      Author: coldwinter
 */

#ifndef AXKEYPADDELEGATE_H_
#define AXKEYPADDELEGATE_H_

#include <cocos2d.h>
#include "AXKeypadDispatcher.h"
 using namespace cocos2d;

class AXKeypadDelegate
{
public:
    AXKeypadDelegate():m_is_enabled(false){}
	// on key down
	virtual bool onKeyDown(int keyCode){return false;}
    // on key up
	virtual bool onKeyUp(int keyCode){return false;}

    virtual void setAXKeypadEnable(bool is_enable)
    {
        if(m_is_enabled == is_enable) return;
        m_is_enabled = is_enable;
        if(is_enable)
        {
            AXKeypadDispatcher::sharedKeypadDispatcher()->registKeypadDispatcher(this);
        }
        else
        {
            AXKeypadDispatcher::sharedKeypadDispatcher()->unregistKeypadDispatcher(this);
        }
    }
    
protected:
    bool m_is_enabled;
};

class AXKeypadHandler:public CCObject
{
public:
	static AXKeypadHandler* createWithDelegate(AXKeypadDelegate*);

	virtual bool initWithDelegate(AXKeypadDelegate* delegate);

	void setDelegate(AXKeypadDelegate* delegate);

	AXKeypadDelegate* getDelegate()
    {return m_keypad_delegate;}
    
    virtual ~AXKeypadHandler();
	
protected:
	AXKeypadDelegate* m_keypad_delegate;
};

#endif /* AXKEYPADDELEGATE_H_ */
