/*
 * AXKeypadDelegate.cpp
 *
 *  Created on: 2014年5月16日
 *      Author: coldwinter
 */

#include "AXKeypadDelegate.h"

AXKeypadHandler* AXKeypadHandler::createWithDelegate(
                             AXKeypadDelegate *delegate)
{
    AXKeypadHandler* handler = new AXKeypadHandler();
    if(handler && handler->initWithDelegate(delegate))
    {
        handler->autorelease();
    }
    else
    {
        delete handler;
        handler = NULL;
    }
    return handler;
}

bool AXKeypadHandler::initWithDelegate(AXKeypadDelegate *delegate)
{
    bool bRet = false;
    do
    {
        CCObject* obj = dynamic_cast<CCObject*>(delegate);
        CC_BREAK_IF(!obj);
        obj->retain();
        m_keypad_delegate = delegate;
        
        bRet = true;
        
    }while(0);
    return bRet;
}

void AXKeypadHandler::setDelegate(AXKeypadDelegate *delegate)
{
    if(m_keypad_delegate)
    {
        ((CCObject*)m_keypad_delegate)->release();
        m_keypad_delegate = NULL;
    }
    CCObject* obj = dynamic_cast<CCObject*>(delegate);
    if(obj)
    {
        obj->retain();
        m_keypad_delegate = delegate;
    }
}

AXKeypadHandler::~AXKeypadHandler()
{
    if(m_keypad_delegate)
    {
        CCObject* obj = dynamic_cast<CCObject*>(m_keypad_delegate);
        obj->release();
        m_keypad_delegate = NULL;
    }

}


