/*
 * AXKeypadImp.cpp
 *
 *  Created on: 2014年5月15日
 *      Author: coldwinter
 */
#include <jni.h>
#include <android/log.h>

#include <cocos2d.h>

#include "AXKeypadDispatcher.h"


#define  LOGE(...)  __android_log_print(ANDROID_LOG_DEBUG,"AssionX Lib",__VA_ARGS__)

using namespace cocos2d;

extern "C"
{
JNIEXPORT void Java_com_assionx_lib_AssionXKeypad_onKeyDown(JNIEnv* env,jobject* obj,jint keyCode)
{
	AXKeypadDispatcher::sharedKeypadDispatcher()->postKeyEvent(keyCode,0);
}

JNIEXPORT void Java_com_assionx_lib_AssionXKeypad_onKeyUp(JNIEnv* env,jobject* obj,jint keyCode)
{
	AXKeypadDispatcher::sharedKeypadDispatcher()->postKeyEvent(keyCode,1);
}

JNIEXPORT void Java_com_assionx_lib_AssionxKeypad_onKeyLongPressed(JNIEnv* env,jobject* obj,jint keyCode)
{
}

}



