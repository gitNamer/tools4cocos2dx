#!/bin/bash

#################################
# 生成android.mk开始
function create_mk
{
# 代码所在根目录
PROJECT_ROOT=$APP_ROOT
# 配置要遍历的文件夹
DIR_LIST="Classes 
Src_android"

# list all cpp file;
cpp=""
# list header dir
header=""

	if [[ -L src_cpp  ]]; then
		rm src_cpp
	fi
	ln -s $PROJECT_ROOT src_cpp

	for CLASS_PATH in $DIR_LIST
	do
		for file in	$(find src_cpp/$CLASS_PATH -follow | grep .cpp$  )
		do
			cpp=${cpp}../${file}" \\
			"
		done

		for file in $(find $PROJECT_ROOT/$CLASS_PATH -type d -follow)
		do
			header=$header$file" \\
			"
		done
	done

	echo "LOCAL_PATH := \$(call my-dir)"
	echo ""
	echo "include \$(CLEAR_VARS)"
	echo ""
	echo "LOCAL_MODULE := game_shared"
	echo ""
	echo "LOCAL_MODULE_FILENAME := libgame"
	echo ""
	echo -e "LOCAL_SRC_FILES := ${cpp}"
	echo ""
	echo -e "LOCAL_C_INCLUDES := ${header}"
	echo ""
	echo "LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static"
	echo ""
	echo "include \$(BUILD_SHARED_LIBRARY)"
	echo ""
	echo "\$(call import-module,CocosDenshion/android) \\"
	echo "\$(call import-module,cocos2dx) \\"
	echo "\$(call import-module,extensions)"
}

create_mk >jni/Android_t.mk

FILE_OLD=$(md5 -q jni/Android.mk)
FILE_NEW=$(md5 -q jni/Android_t.mk)
# 比较新旧文件的MD5，若不相等则覆盖
if [ "$FILE_OLD" = "$FILE_NEW" ] ; then
	rm jni/Android_t.mk
	echo "no new file added"
else
	rm jni/Android.mk
	mv jni/Android_t.mk jni/Android.mk
fi

# 生成android.mk完成
################################