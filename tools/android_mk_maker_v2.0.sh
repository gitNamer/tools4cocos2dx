# 配置源文件路径，一行一个文件夹
SOURCECODE_PATHS="../Classes
../Src_android"
# 配置资源路径，没有资源文件则留空即可
ASSETS_PATH=""

# 清空jni目录的链接
function clean_jni
{
	for i in jni/*
	do
		if [ -L $i ]; then
			rm $i
		fi
	done
}

# 创建连接
function link_source_code
{
	for path in $SOURCECODE_PATHS
	do
		ln -s $path jni/${path##*/}
	done
	if [ "$ASSETS_PATH" = "" ]; then
		# do nothing
		echo ""
	else
		rm assets
		ln -s $ASSETS_PATH assets
	fi
}

# 遍历目录中的cpp文件
function list_file
{
	cd jni
	for file in $(find * -follow -type f | grep \\.cpp$ | grep -v \\/\\.)
	do
		echo "$file \\"
	done
	cd ..
}

# 增加头文件目录
function list_include
{
	cd jni
	for path in $(find * -follow -type d | grep -v \\/\\.)
	do
		echo "$(pwd)/$path \\"
	done
	cd ..
}

#创建mk文件
function create_mk
{
	clean_jni
	link_source_code
	CPP=$(list_file)
	HEADER=$(list_include)

	echo "LOCAL_PATH := \$(call my-dir)"
	echo ""
	echo "include \$(CLEAR_VARS)"
	echo ""
	echo "LOCAL_MODULE := game_shared"
	echo ""
	echo "LOCAL_MODULE_FILENAME := libgame"
	echo ""
	echo "LOCAL_SRC_FILES := ${CPP}"
	echo ""
	echo "LOCAL_C_INCLUDES := ${HEADER}"
	echo ""
	echo "LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static"
	echo ""
	echo "include \$(BUILD_SHARED_LIBRARY)"
	echo ""
	echo "\$(call import-module,CocosDenshion/android) \\"
	echo "\$(call import-module,cocos2dx) \\"
	echo "\$(call import-module,extensions)"
}

ANDROID_MK="$(create_mk)"
FILE_OLD="$(cat jni/Android.mk)"
FILE_NEW=$ANDROID_MK

if [ "$FILE_OLD" = "$FILE_NEW" ]; then
	echo "equal do nothing"
else
	echo "not equal replace"
	echo "$ANDROID_MK" > jni/Android.mk
fi
