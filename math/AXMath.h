//
//  AXMath.h
//  AssionX
//
//  Created by Coldwinter on 14-2-27.
//
//  AX数学工具类

#ifndef __AssionX__AXMath__
#define __AssionX__AXMath__

#include "cocos2d.h"

using namespace cocos2d;

#define AX_IS_BETWEEN(__NUM__,__START__,__END__) \
( (__NUM__) >= (__START__) && (__NUM__) <= (__END__) )

#define AX_MAX(__NUM1__,__NUM2__) \
(__NUM1__)>(__NUM2__)?(__NUM1__):(__NUM2__)

#define AX_MIN(__NUM1__,__NUM2__) \
(__NUM1__)<(__NUM2__)?(__NUM1__):(__NUM2__)

class AXMath
{
public:
    // random a value between the start and the end
    static float random(float start,float end);
    // random a value between the start and the end
    static int random(int start,int end);
    // Generating a point within a specified rect
    static inline CCPoint randomPointIn(CCRect &rect)
    {
        if(rect.equals(CCRectZero)) return CCPointZero;
        return ccp( random(rect.getMinX(),rect.getMaxX()), 
        	random(rect.getMinY(), rect.getMaxY()));
    };
    
};

#endif /* defined(__AssionX__AXMath__) */
