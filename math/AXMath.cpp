//
//  AXMath.cpp
//  AssionX
//
//  Created by Coldwinter on 14-2-27.
//
//

#include "AXMath.h"
#include <stdlib.h>
#include <math.h>

float AXMath::random(float start, float end)
{
    return (rand()/(float)RAND_MAX) * fabsf(end - start) + fminf(start, end);
}

int AXMath::random(int start, int end)
{
    return rand() % abs(end - start) + (start>end?end:start);
}
